﻿using ImageMosaic;
using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace OnEarthWindowsForms
{
    public partial class Form1 : Form
    {
        string file = string.Empty;
        string imagesDirectory = string.Empty;
        string fileNameResult = string.Empty;
        System.Drawing.Imaging.ImageFormat resultImageFormat = System.Drawing.Imaging.ImageFormat.Png;
        public Form1()
        {
            InitializeComponent();
            imagesDirectory = string.Concat(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName, @"\Images");
            fileNameResult = string.Concat("mosaic.", resultImageFormat.ToString().ToLower());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
                file = openFileDialog1.FileName;
            else
                label1.Text = "Error opening image";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(file) && !string.IsNullOrEmpty(imagesDirectory))
            {
                Mosaic mosaic = new MosaicGenerator().Generate(file, imagesDirectory);
                Bitmap imageResult = new Bitmap(mosaic.Image);
                imageResult.Save(fileNameResult, resultImageFormat);
                Close();
            }
        }
    }
}
