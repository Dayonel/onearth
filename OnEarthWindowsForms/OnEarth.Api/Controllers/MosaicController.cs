﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using System;

namespace OnEarth.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MosaicController : Controller
    {
        private readonly IHostingEnvironment _environment;
        string imagesFolderPath = "Images";
        string sourceImageFolderPath = "SourceImage";
        public MosaicController(IHostingEnvironment environment)
        {
            _environment = environment ?? throw new ArgumentNullException(nameof(environment));
        }

        // GET api/values
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost("UploadFile")]
        public async void Post(IFormFile file)
        {
            var uploads = Path.Combine(_environment.WebRootPath, sourceImageFolderPath);
            if (file.Length > 0)
                using (var fileStream = new FileStream(Path.Combine(uploads, file.FileName), FileMode.Create))
                    await file.CopyToAsync(fileStream);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
